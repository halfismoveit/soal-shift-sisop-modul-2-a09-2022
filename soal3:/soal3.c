#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>

char *zip_path = "/home/hapis/modul2/animal.zip";
char *path = "/home/hapis/modul2/";
char list_air[100][100];
int amount_air = 0;

void copy(char *source, char *path); // meng-copy file dari suatu direktori ke direktori lain
void delete(char *file); // menghapus file
void animal_type(char *path); // mengelompokkan hewan darat dan air
void delete_bird(char *path); // manghapus file yang mengandung kata "bird"
void list(); // membuat daftar file di direktori hewan air beserta UID dan user permissionnya

int main() {
	pid_t child_id, child_id2, child_id3, child_id4; // parent
	int status;

	child_id = fork();
	  
	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if (child_id == 0) {
		// this is child
		child_id2 = fork();
		int status2;
		    
		if (child_id2 == 0) {
			char *argv[] = {"mkdir", "-p", "/home/hapis/modul2/darat", NULL}; 
		    	execv("/bin/mkdir", argv); // membuat direktori untuk hewan darat
		}
		else {
			while ((wait(&status2)) > 0);
		    	sleep(3);
		    	char *argv[] = {"mkdir", "-p", "/home/hapis/modul2/air", NULL};
		    	execv("/bin/mkdir", argv); // membuat direktori untuk hewan laut
		}
	} 
	  
	else {
	    // this is parent
	    	while ((wait(&status)) > 0); // menunggu kedua proses membuat direktori selesai
	    	child_id2 = fork();
	    	int status2;
	    
	    	if (child_id2 == 0) {
	    		child_id3 = fork();
	    		int status3;
	    	
	    		if (child_id3 == 0) {
				char *argv[] = {"unzip", "-j", zip_path, "*jpg", "-d", path, NULL};
	    			execv("/bin/unzip", argv); // melakukan unzip
	    		}
	    		else {
	    			while ((wait(&status3)) > 0);
	    	    		animal_type(path); // memanggil fungsi untuk mengelompokkan jenis hewan
	    		}
	    	}
		else {
	    		while ((wait(&status2)) > 0); // menunggu proses unzip dan pengelompokan selesai
	    		child_id3 = fork();
	    		int status3;
	    	
	    		if (child_id3 == 0) {
	    			delete_bird("/home/hapis/modul2/darat/"); // menghapus file dengan kata "bird"
	    		}
			else {
				while ((wait(&status3)) > 0);
				list(); // membuat daftar file dan detailnya di dalam "list.txt"		
			}

		}
	}
return 0;
}

void copy(char *source, char *path){
    	int status; 
    	pid_t child_id = fork();
    
    	if(child_id == 0){
        	char *argv[] = {"cp", "-n", source, path, NULL};
        	execv("/bin/cp", argv);
    	} else {
        	((wait(&status)) > 0);
    	}
}


void delete(char *file){
    	int status; 
    	pid_t child_id = fork();
    
    	if(child_id == 0){
        	char *argv[] = {"rm", "-d", file, NULL};
        	execv("/bin/rm", argv);
    	} else {
        	((wait(&status)) > 0);
    	}
}

void animal_type(char *path){
    	struct dirent *dp;
    	DIR *dir = opendir(path);

    	while((dp = readdir(dir)) != NULL){
        	if(strcmp(dp->d_name, "darat") == 0 || strcmp(dp->d_name, "air") == 0 || 
        	strcmp(dp->d_name, "animal.zip") == 0 || strcmp(dp->d_name, ".") == 0 ||
        	strcmp(dp->d_name, "..") == 0 ) continue;
        
        	if(strstr(dp->d_name, "darat")){
		    	char dest[1000] = "/home/hapis/modul2/darat/";
		    	char src[1000] = "/home/hapis/modul2/";
		    	strcat(dest, dp->d_name);
		    	strcat(src, dp->d_name);
		    	copy(src, dest);
        	}

        	if(strstr(dp->d_name, "air")){
		    	char dest[1000] = "/home/hapis/modul2/air/";
		    	char src[1000] = "/home/hapis/modul2/";
		    	strcat(dest, dp->d_name);
		    	strcat(src, dp->d_name);
		    	copy(src, dest);
        	}
        
        char del[1000] = "/home/hapis/modul2/";
        strcat(del, dp->d_name);
        delete(del);
    	}
}

void delete_bird(char *path){
    	struct dirent *dp;
    	DIR *dir = opendir(path);
    
    	while((dp = readdir(dir)) != NULL){
			if (strstr(dp->d_name, "bird")) {
			    	char del[100] = "/home/hapis/modul2/darat/";
			    	strcat(del, dp->d_name);
			    	delete(del);
			}
    	}
}

void list() {
	DIR *dp;
	struct dirent *ep;
	char path_air[100] = "/home/hapis/modul2/air/";
	dp = opendir(path_air);

  	struct stat fs;
  	char file_permission[5];
  	int r;
  
  	uid_t uid = getuid();
  	struct passwd *pw = getpwuid(uid);
  
  	r = stat(path_air,&fs);
  
  	if (r == -1)
  	{
    		fprintf(stderr, "File error\n");
    		exit(1);
  	}
  
  	if ( fs.st_mode & S_IRUSR )
    		file_permission[0] = 'r';
  	if ( fs.st_mode & S_IWUSR )
    		file_permission[1] = 'w';
  	if ( fs.st_mode & S_IXUSR )
    		file_permission[2] = 'x';

	if (dp != NULL)
    	{
      		while ((ep = readdir (dp))) {
			if (pw != 0)
  			{
  				if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
  				
    				FILE *list_air;
    				list_air = fopen("/home/hapis/modul2/air/list.txt", "a");
    				fprintf(list_air, "%s_%s_%s\n", pw->pw_name, file_permission, ep->d_name);
    				fclose(list_air);
    				}
  			}
      	}
      		(void) closedir (dp);
    	} else perror ("Couldn't open the directory");
}
