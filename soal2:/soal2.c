#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

char *path = "/home/hapis/shift2/drakor";
char *zip_path = "/home/hapis/Downloads/drakor.zip";
char tmp[100], new_file[100], old_file[100];

void category();
void init_file(char *path, char *genre);
void insert_file(char *path, char *nama, char *tahun);

int main() {
  	pid_t child_id, child_id2;
	int status;

  	child_id = fork();
  
  	if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
    		// this is child
    		child_id2 = fork();
    		int status2;
    
    		if (child_id2 == 0) {
    			char *argv[] = {"mkdir","-p",path,NULL};
    			execv("/bin/mkdir",argv);
    		}
    		else {
    			while ((wait(&status2)) > 0);
    			char *argv[] = {"unzip",zip_path,"-d",path,"-x","*/*",NULL};
    			execv("/bin/unzip",argv);
    		}
  	} 
  	
  	else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		category();
  	}
  	return 0;
}

void category() {
	pid_t child_id3, child_id4;
	int status4;

    	DIR *dp, *ep;
    	struct dirent *fp;
    	char file_name[100], temp_path[100], temp[100];
    	dp = opendir(path);
    
    	if (dp != NULL) {
        	while ((fp = readdir(dp)) != NULL) {
        		if (strstr(fp->d_name,".png")) { // jika filenya memiliki unsur ".png"
		     		strcpy(file_name, fp->d_name);
		     		strcpy(temp, fp->d_name);
		     		strcpy(temp_path, path);
		     		
		     		// menghapus ".png"
		     		int l = strlen(temp);
		     		for(int i = l; i >= l - 4; i--) {
		     			temp[i] = '\0';
		     		}
		     		
		     		char *token = strtok(temp, ";_");
		     		
		     		if (strstr(fp->d_name,"_")) {
				 	int count = 0;
				 	
				 	char string[6][100];
				 	
				 	while(token != NULL){		         		
				 		strcpy(string[count],token);
				 		token = strtok(NULL, ";_");
						count++;
				 	}
				 	  				      		
					strcpy(old_file, path);
					strcat(old_file, "/");
					strcat(old_file, file_name);
					      	     
					strcat(temp_path,"/");
				 	strcat(temp_path,string[2]);
				 		
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; // membuat direktori genre
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[2]); // membuat file data.txt
						}
					}
								
					insert_file(temp_path, string[0], string[1]); // memasukkan nama dan tahun
					      	
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[0]);
					strcat(new_file, ".png");

					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"cp", old_file, new_file, NULL}; 
		    				execv("/bin/cp", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}
					      	      	
					strcpy(temp_path, path);
					strcat(temp_path,"/");
				 	strcat(temp_path,string[5]);
				 		
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; 
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[5]);
						}
					}
					
					insert_file(temp_path, string[3], string[4]);
						
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[3]);
					strcat(new_file, ".png");

					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"mv", old_file, new_file, NULL}; 
		    				execv("/bin/mv", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}
							
		         	}
		         	else {
				 	int count = 0;
				 	
				 	char string[3][100];
				 	while(token != NULL){
				 		strcpy(string[count],token);
				 		token = strtok(NULL, ";_");
						count++;
				 	}
				 	strcat(temp_path,"/");
				 	strcat(temp_path,string[2]);
				 
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; 
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[2]);
						}
					}
					
					insert_file(temp_path, string[0], string[1]);
					      	
					strcpy(old_file, path);
					strcat(old_file, "/");
					strcat(old_file, file_name);
					      	
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[0]);
					strcat(new_file, ".png");
						
					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"mv", old_file, new_file, NULL}; 
		    				execv("/bin/mv", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}						
				 }
		     	}
		}
	closedir(dp);
    	}
}

void init_file(char *path, char *genre){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/data.txt");
	fp = fopen (tmp, "a+");
	fprintf(fp, "kategori : %s", genre);
	fclose(fp);
}

void insert_file(char *path, char *nama, char *tahun){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/data.txt");
	fp = fopen (tmp, "a+");
	fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", nama, tahun);
	fclose(fp);
}
