#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>

char linkcharacter[100]={"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"};
char linkweapon[100]={"https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

int main() {
  	pid_t child_id, child_id2, child_id3;
  	int status;

  	child_id = fork();
  
  	if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
    		// this is child
    
    		child_id2 = fork();
    		int status2;
    
    		if (child_id2 == 0) {
    
        		child_id3 = fork();
        		int status3;
        
             		if (child_id3 == 0) {
             			// membuat direktori gacha_gacha
                 		char *argv[] = {"mkdir", "-p", "/home/hapis/gacha_gacha", NULL};
    	          		execv("/bin/mkdir", argv);  
    	      		}
           		else {
    				while ((wait(&status3))>0);
    				// mengunduh dari google drive untuk karakter
        			execlp("wget","wget","--no-check-certificate",linkcharacter,"-q","-O","Anggap_ini_database_characters.zip", NULL );
    			}
    		}
    		else {
    			while ((wait(&status2)) > 0);
    			// mengunduh dari google drive untuk senjata
    			execlp("wget","wget","--no-check-certificate",linkweapon,"-q","-O","Anggap_ini_database_weapon.zip", NULL );
    		}
  	} 
  	else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		
    		child_id2 = fork();
    		int status2;
    		
    		if (child_id2 == 0) {
    			// membuka zip database karakter
    			execlp("unzip","unzip","Anggap_ini_database_characters.zip",NULL);
    		}
    		else {
    			while ((wait(&status2)) > 0);
    			// membuka zip database senjata
    			execlp("unzip","unzip","Anggap_ini_database_weapon.zip",NULL);
    		}
    
  	}
  	return 0;
}
