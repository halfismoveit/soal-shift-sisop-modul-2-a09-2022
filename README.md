# soal-shift-sisop-modul-2-A09-2022

Project Soal Shift Modul 2 Mata Kuliah Sistem Operasi Tahun 2022

## Soal 1
### 
Pertama, kita perlu mengunduh sebuah file zip dari https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view dan https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view. Kemudian, kedua file akan diekstrak dan dimasukkan ke dalam sebuah folder bernama "gacha_gacha". Dikarenakan adanya aturan tidak boleh menggunakan "system()", "mkdir()", dan "rename()", kita dapat menggunakan "exec" sebagai gantinya untuk membuat sebuah "directory" bernama "gacha_gacha".

```
char linkcharacter[100]={"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"};
char linkweapon[100]={"https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

```


## Soal 2
### 
Inisialisasi array dan fungsi.
```
char *path = "/home/hapis/shift2/drakor";
char *zip_path = "/home/hapis/Downloads/drakor.zip";
char tmp[100], new_file[100], old_file[100];

void category();
void init_file(char *path, char *genre);
void insert_file(char *path, char *nama, char *tahun);
```
Di "main", dengan "execv" akan dibuat direktori "drakor" dan unzip "drakor.zip". Kemudian, akan memanggil fungsi untuk mengelompokkan drama.
```
int main() {
  	pid_t child_id, child_id2;
	int status;

  	child_id = fork();
  
  	if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
    		// this is child
    		child_id2 = fork();
    		int status2;
    
    		if (child_id2 == 0) {
    			char *argv[] = {"mkdir","-p",path,NULL};
    			execv("/bin/mkdir",argv);
    		}
    		else {
    			while ((wait(&status2)) > 0);
    			char *argv[] = {"unzip",zip_path,"-d",path,"-x","*/*",NULL};
    			execv("/bin/unzip",argv);
    		}
  	} 
  	
  	else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		category();
  	}
  	return 0;
}
```
 Proses pengelompokannya adalah:
1. Inisialisasi
2. Menghilangkan ".png"
3. Dengan "strtok", string dibagi menjadi nama, tahun, kategori
4. Buat direktori sesuai kategori
5. Buat "data.txt"
6. Insert
```
void category() {
	pid_t child_id3, child_id4;
	int status4;

    	DIR *dp, *ep;
    	struct dirent *fp;
    	char file_name[100], temp_path[100], temp[100];
    	dp = opendir(path);
    
    	if (dp != NULL) {
        	while ((fp = readdir(dp)) != NULL) {
        		if (strstr(fp->d_name,".png")) { // jika filenya memiliki unsur ".png"
		     		strcpy(file_name, fp->d_name);
		     		strcpy(temp, fp->d_name);
		     		strcpy(temp_path, path);
		     		
		     		// menghapus ".png"
		     		int l = strlen(temp);
		     		for(int i = l; i >= l - 4; i--) {
		     			temp[i] = '\0';
		     		}
		     		
		     		char *token = strtok(temp, ";_");
		     		
		     		if (strstr(fp->d_name,"_")) {
				 	int count = 0;
				 	
				 	char string[6][100];
				 	
				 	while(token != NULL){		         		
				 		strcpy(string[count],token);
				 		token = strtok(NULL, ";_");
						count++;
				 	}
				 	  				      		
					strcpy(old_file, path);
					strcat(old_file, "/");
					strcat(old_file, file_name);
					      	     
					strcat(temp_path,"/");
				 	strcat(temp_path,string[2]);
				 		
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; // membuat direktori genre
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[2]); // membuat file data.txt
						}
					}
								
					insert_file(temp_path, string[0], string[1]); // memasukkan nama dan tahun
					      	
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[0]);
					strcat(new_file, ".png");

					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"cp", old_file, new_file, NULL}; 
		    				execv("/bin/cp", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}
					      	      	
					strcpy(temp_path, path);
					strcat(temp_path,"/");
				 	strcat(temp_path,string[5]);
				 		
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; 
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[5]);
						}
					}
					
					insert_file(temp_path, string[3], string[4]);
						
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[3]);
					strcat(new_file, ".png");

					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"mv", old_file, new_file, NULL}; 
		    				execv("/bin/mv", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}
							
		         	}
		         	else {
				 	int count = 0;
				 	
				 	char string[3][100];
				 	while(token != NULL){
				 		strcpy(string[count],token);
				 		token = strtok(NULL, ";_");
						count++;
				 	}
				 	strcat(temp_path,"/");
				 	strcat(temp_path,string[2]);
				 
				 	ep = opendir(temp_path);
					if (ep == NULL) {
						child_id3 = fork();
						int status3;
						
						if (child_id3 == 0) {
							char *argv[] = {"mkdir", "-p", temp_path, NULL}; 
		    					execv("/bin/mkdir", argv);
						}
						else {
							while((wait(&status3)) > 0);
							init_file(temp_path, string[2]);
						}
					}
					
					insert_file(temp_path, string[0], string[1]);
					      	
					strcpy(old_file, path);
					strcat(old_file, "/");
					strcat(old_file, file_name);
					      	
					strcpy(new_file, temp_path);
					strcat(new_file, "/");
					strcat(new_file, string[0]);
					strcat(new_file, ".png");
						
					child_id4 = fork();
						
					if (child_id4 == 0) {
						char *argv[] = {"mv", old_file, new_file, NULL}; 
		    				execv("/bin/mv", argv);
					}
					else {
						while((wait(&status4)) > 0);
					}						
				 }
		     	}
		}
	closedir(dp);
    	}
}
```
## Soal 3
### Penjelasan soal
Pertama, kita akan membuat dua direktori, yakni "darat" dan "air". Kedua direktori tersebut memiliki jeda pembuatan 3 detik yang dapat dicapai menggunakan "sleep". Kemudian, kita akan mengekstrak "animal.zip" yang berada di /home/[USER]/modul2/. Lalu klarifikasikan hewan berdasarkan nama file lalu masukkan ke dalam folder yang sesuai pada fungsi klarifikasi memerlukan 2 fungsi lain yaitu copy dan delete fungsi copy. klarifikasi berdasarkan nama ini menggunakan strstr jadi apabila tidak ada yang keterangan nantinya akan dihapus tanpa di-copy. Lalu kita menghapus semua nama file yang ada kata bird. untuk permintaan terakhir yaitu membuat file list.txt yang berisi nama hewan dari folder air dengan format nama UID_[UID file permission]_Nama File.[jpg/png]. 

### A. Membuat Folder
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.Untuk dapat membuat folder dengan menggunakan bahasa c digunakanlah execv untuk menjalankan argumen sebagai command lalu digunakan -p agar jika parent folder belom dibuat maka parent folder akan dibuat juga. Lalu agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan digunakan "while(wait(&status)) > 0".
### 1. Membuat folder darat
```
char *argv[] = {"mkdir", "-p", "/home/hapis/modul2/darat", NULL}; 
		    	execv("/bin/mkdir", argv); // membuat direktori untuk hewan darat

```
![1](images/3.png)
### 2. Membuat folder air setelah 3 detik
```
while ((wait(&status2)) > 0);
		    	sleep(3);
		    	char *argv[] = {"mkdir", "-p", "/home/hapis/modul2/air", NULL};
		    	execv("/bin/mkdir", argv); // membuat direktori untuk hewan laut
```
![1](images/3a.png)
### B. Melakukan Unzip Pada File animal.zip
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.command unzip pada script c digunakanlah execlp diisi dengan command yang ingin dijalankan yaitu unzip dan membuat destinasi untuk hasil dari unzip. Selanjutnya command juga digunakan agar tidak menghasilkan verbose pada terminal.
```
char *argv[] = {"unzip", "-j", zip_path, "*jpg", "-d", path, NULL};
	    			execv("/bin/unzip", argv); // melakukan unzip

```
![1](images/3b.png)
 Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
 while ((wait(&status3)) > 0);
```

### C.Memindahkan file dari animal.zip ke hewan darat dan hewan air
hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.setiap iterasi file pada folder dilakukan pengcopyan nama file (dir->d_name) ke array global hewan serta menambahkan sebanyak satu pada variabel count untuk menghitung berapa banyak jumlah file yang ada pada folder yang dibuka. Lalu jika sudah selesai melakukan iterasi pada folder, folder akan ditutup. Lalu dilakukanlah penglistan hewan sesuai kategori yang ada yaitu "air" dan "darat" dengan melakukan iterasi sebanyak jumlah hewan yang ada. menggunakan fungsi copy
```
void copy(char *source, char *path){
    	int status; 
    	pid_t child_id = fork();
    
    	if(child_id == 0){
        	char *argv[] = {"cp", "-n", source, path, NULL};
        	execv("/bin/cp", argv);
    	} else {
        	((wait(&status)) > 0);
    	}
}

```
![1](images/3c.png)
```
void animal_type(char *path){
    	struct dirent *dp;
    	DIR *dir = opendir(path);

    	while((dp = readdir(dir)) != NULL){
        	if(strcmp(dp->d_name, "darat") == 0 || strcmp(dp->d_name, "air") == 0 || 
        	strcmp(dp->d_name, "animal.zip") == 0 || strcmp(dp->d_name, ".") == 0 ||
        	strcmp(dp->d_name, "..") == 0 ) continue;
        
        	if(strstr(dp->d_name, "darat")){
		    	char dest[1000] = "/home/hapis/modul2/darat/";
		    	char src[1000] = "/home/hapis/modul2/";
		    	strcat(dest, dp->d_name);
		    	strcat(src, dp->d_name);
		    	copy(src, dest);
        	}

        	if(strstr(dp->d_name, "air")){
		    	char dest[1000] = "/home/hapis/modul2/air/";
		    	char src[1000] = "/home/hapis/modul2/";
		    	strcat(dest, dp->d_name);
		    	strcat(src, dp->d_name);
		    	copy(src, dest);
        	}
        
        char del[1000] = "/home/hapis/modul2/";
        strcat(del, dp->d_name);
        delete(del);
    	}
}
```
![1](images/3c2.png)

Untuk hewan yang tidak ada keterangan air atau darat harus dihapus Untuk dapat menghapus file serta folder yang tidak dibutuhkan yaitu animal dan file yang tidak sesuai dengan format dapat dilakukan langsung dengan rm -r ("recursive") path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
void delete(char *file){
    	int status; 
    	pid_t child_id = fork();
    
    	if(child_id == 0){
        	char *argv[] = {"rm", "-d", file, NULL};
        	execv("/bin/rm", argv);
    	} else {
        	((wait(&status)) > 0);
    	}
}
```
### D.Menghapus file yang memiliki kata "Burung" dalam foler bernama "darat
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.Untuk dapat menghapus file yang diinginkan dapat dilakukan langsung dengan rm(fungsi delete).
```
void delete_bird(char *path){
    	struct dirent *dp;
    	DIR *dir = opendir(path);
    
    	while((dp = readdir(dir)) != NULL){
			if (strstr(dp->d_name, "bird")) {
			    	char del[100] = "/home/hapis/modul2/darat/";
			    	strcat(del, dp->d_name);
			    	delete(del);
			}
    	}
}
```
![1](images/3d.png)
### E.Melakukan list untuk nama file dari folder air ke list.txt
membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.Contoh : conan_rwx_hewan.png.melakukan check stat pada folder air lalu digunakanlah getpwuid dari path yang diisi, lalu untuk mendapat nama dapat dengan mudah dilakukan pw->pw_name. UID Permission didapat dengan membuat char array dan dicek satu persatu untuk read, write, dan execute dengan S_IRUSR(untuk r), S_IRUSR(untuk w), dan S_IRUSR(untuk x).Open file dengan mode "a" (append) dan untuk menulis per line digunakan fprintf dengan format yang telah diminta. Lalu setelah selesai menggunakan file, akan dilakukan penutupan file agar tidak leak
```
void list() {
	DIR *dp;
	struct dirent *ep;
	char path_air[100] = "/home/hapis/modul2/air/";
	dp = opendir(path_air);

  	struct stat fs;
  	char file_permission[5];
  	int r;
  
  	uid_t uid = getuid();
  	struct passwd *pw = getpwuid(uid);
  
  	r = stat(path_air,&fs);
  
  	if (r == -1)
  	{
    		fprintf(stderr, "File error\n");
    		exit(1);
  	}
  
  	if ( fs.st_mode & S_IRUSR )
    		file_permission[0] = 'r';
  	if ( fs.st_mode & S_IWUSR )
    		file_permission[1] = 'w';
  	if ( fs.st_mode & S_IXUSR )
    		file_permission[2] = 'x';

	if (dp != NULL)
    	{
      		while ((ep = readdir (dp))) {
			if (pw != 0)
  			{
  				if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
  				
    				FILE *list_air;
    				list_air = fopen("/home/hapis/modul2/air/list.txt", "a");
    				fprintf(list_air, "%s_%s_%s\n", pw->pw_name, file_permission, ep->d_name);
    				fclose(list_air);
    				}
  			}
      	}
      		(void) closedir (dp);
    	} else perror ("Couldn't open the directory");
}
```
![1](images/3e.png)
### Catatan Soal nomor 3
Tidak boleh memakai system().
Tidak boleh memakai function C mkdir() ataupun rename().
Gunakan exec dan fork
Direktori “.” dan “..” tidak termasuk

## Kendala
User ID bisa bernama daemon
